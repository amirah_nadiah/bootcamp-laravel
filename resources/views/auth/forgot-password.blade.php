@extends('layout.public')

@section('content')
    <div>
    <h1 class="h4 text-gray-900 mb-2">Forgot Your Password?</h1>
  <p class="mb-4">We get it, stuff happens. Just enter your email address below
                                            and we'll send you a link to reset your password!</p>    </div>

    @if (session('status'))
        <div>
            {{ session('status') }}
        </div>
    @endif

    @if ($errors->any())
        <div>
            <div>{{ __('Whoops! Something went wrong.') }}</div>

            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" class="user" action="{{ route('password.email') }}">
        @csrf

        <div class="form-group">
            <label>{{ __('Email') }}</label>
            <input type="email" name="email" class="form-control form-control-user" placeholder="Enter Email Address..." value="{{ old('email') }}" required autofocus />
        </div>

        <div>
            <button type="submit" class="btn btn-primary btn-user btn-block">
                {{ __('Reset Password') }}
            </button>
        </div>
    </form>
@endsection
